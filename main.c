#include "stm32l1xx.h"
#include "delay.h"

int main() {
  delay_init();

  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;  // Enable GPIOB
  GPIOB->MODER = GPIO_MODER_MODER6_0; // Output mode
  GPIOB->OSPEEDR = GPIO_OSPEEDER_OSPEEDR6_0;  // Medium speed
  while(1) {
    GPIOB->BSRR = GPIO_BSRR_BS_6;
    _delay_ms(30000);
    GPIOB->BSRR = GPIO_BSRR_BR_6;
    _delay_ms(30000);
  }

  return 0;
}